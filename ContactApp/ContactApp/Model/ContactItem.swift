//
//  ContactItem.swift
//  ContactApp
//
//  Created by Adi Wibowo P on 6/26/19.
//  Copyright © 2019 Adi Wibowo P. All rights reserved.
//
import Foundation
import SwiftyJSON

class ContactItem: NSObject {
    var id : String?
    var firstName: String?
    var lastName: String?
    var age: Int = 0
    var photo: String?
    
    static func with(json: JSON) -> ContactItem? {
        let contact = ContactItem()
        
        if json["id"].exists() {
            contact.id = json["id"].string
        }
        
        if json["firstName"].exists() {
            contact.firstName = json["firstName"].string
        }
        
        if json["lastName"].exists() {
            contact.lastName = json["lastName"].string
        }
        
        if json["age"].exists() {
            contact.age = json["age"].intValue
        }
        
        if json["photo"].exists() {
            contact.photo = json["photo"].string
        }
        
        return contact
    }
    
}
