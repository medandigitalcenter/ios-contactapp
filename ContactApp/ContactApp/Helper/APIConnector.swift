//
//  APIConnector.swift
//  ContactApp
//
//  Created by Adi Wibowo P on 6/26/19.
//  Copyright © 2019 Adi Wibowo P. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import SwiftyJSON

struct APIResponse {
    var code: Int
    var message: String
    var result: JSON
    
    init(code: Int, message: String, result: JSON) {
        self.code = code
        self.message = message
        self.result = result
    }
}

class APIConnector: NSObject {
    static let instance = APIConnector()
    let manager: APIManager
    let homeURLString : String
    
    private let API_GET_CONTACT = "/contact"
    private let API_SAVE_CONTACT = "/contact"
    private let API_UPDATE_CONTACT = "/contact"
    private let API_DELETE_CONTACT = "/contact"
    private let API_DETAIL_CONTACT = "/contact"
    
    override init() {
        homeURLString = "https://simple-contact-crud.herokuapp.com"
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        manager = APIManager(configuration: configuration)
        super.init()
    }
    
    func getListContact() -> Observable<[ContactItem]> {
        let request = manager.request(homeURLString + API_GET_CONTACT, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
        return request.rx_JSON()
            .mapJSONResponse()
            .map { response in
                var contacts = [ContactItem]()
                for contact in response.result.arrayValue {
                    if let con = ContactItem.with(json: contact) {
                        contacts.append(con)
                    }
                }
                
                return contacts
        }
    }
    
    func saveContact(contact : ContactItem) -> Observable<Void> {
        print("GO")
        var parameters = [String:Any]()
        if let fN = contact.firstName {
            parameters["firstName"] = fN
        }
        
        if let lN = contact.lastName {
            parameters["lastName"] = lN
        }
        
        if contact.age != 0 {
            parameters["age"] = contact.age
        }
        
        let request = manager.request(homeURLString + API_SAVE_CONTACT, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil)
        print("Isi Param \(parameters)")
        return request.rx_JSON()
            .mapJSONResponse()
            .map { response in
                if response.code < 201 {
                    return Void()
                }
                throw NSError(domain: "APIErrorDomain", code: -1, userInfo: [NSLocalizedDescriptionKey: response.message])
        }
    }
    
    func updateContact(contact : ContactItem) -> Observable<ContactItem> {
        print("GO")
        var parameters = [String:Any]()
        if let fN = contact.firstName {
            parameters["firstName"] = fN
        }
        
        if let lN = contact.lastName {
            parameters["lastName"] = lN
        }
        
        if let pt = contact.photo {
            parameters["photo"] = pt
        }
        
        if contact.age != 0 {
            parameters["age"] = contact.age
        }
        var cId = ""
        if let id = contact.id  {
            cId = id
        }
        let request = manager.request(homeURLString + API_UPDATE_CONTACT + "/\(cId)", method: .put, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil)
        print("Isi Param \(parameters)  \(homeURLString + API_SAVE_CONTACT + cId)")
        return request.rx_JSON()
            .mapJSONResponse()
            .map { response in
                if response.code < 201 {
                    if let c = ContactItem.with(json: response.result) {
                    return c
                    }
                }
                throw NSError(domain: "APIErrorDomain", code: -1, userInfo: [NSLocalizedDescriptionKey: response.message])
        }
    }
    
    func detailContact(contact : ContactItem) -> Observable<ContactItem> {
        
        var cId = ""
        if let id = contact.id  {
            cId = id
        }
        let request = manager.request(homeURLString + API_DETAIL_CONTACT + "/\(cId)", method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: nil)
        print("Isi Param \(homeURLString + API_DETAIL_CONTACT + cId)")
        return request.rx_JSON()
            .mapJSONResponse()
            .map { response in
                if response.code < 201 {
                    if let c = ContactItem.with(json: response.result) {
                        return c
                    }
                }
                throw NSError(domain: "APIErrorDomain", code: -1, userInfo: [NSLocalizedDescriptionKey: response.message])
        }
    }
    
    func deleteContact(contact : ContactItem) -> Observable<Void> {
        var cId = ""
        if let id = contact.id  {
            cId = id
        }
        let request = manager.request(homeURLString + API_DELETE_CONTACT + "/\(cId)", method: .delete, parameters: nil, encoding: URLEncoding.httpBody, headers: nil)
        print("Isi Param   \(homeURLString + API_DELETE_CONTACT + cId)")
        return request.rx_JSON()
            .mapJSONResponse()
            .map { response in
                print("Response Code \(response.code)")
                if response.code < 201 && !response.message.contains("contact unavailable"){
                    return Void()
                }
                throw NSError(domain: "APIErrorDomain", code: -1, userInfo: [NSLocalizedDescriptionKey: response.message])
        }
    }
}

class APIManager: SessionManager {
    
    override func request(_ url: URLConvertible, method: HTTPMethod, parameters: Parameters?, encoding: ParameterEncoding, headers: HTTPHeaders?) -> DataRequest {
        var overridedParameters = [String : AnyObject]()
        let overridedHeaders = [String: String]()
        
        if let parameters = parameters {
            overridedParameters = parameters as [String : AnyObject]
        }
        
        return super.request(url, method: method, parameters: overridedParameters, encoding: encoding, headers: overridedHeaders)
    }
    
}

extension Observable {
    func mapJSONResponse() -> Observable<APIResponse> {
        return map { (item: E) -> APIResponse in
            guard let json = item as? JSON else {
                fatalError("Not a JSON")
            }
            print("to be mapped: ", json)
            var code = 200;
            var message = "";
            var result = json;
            if json["error"].exists(){
                if json["statusCode"].exists() {
                    code = json["statusCode"].intValue
                }
            } else if json["data"].exists() {
                result = json["data"]
            } else {
                result = json
            }
            if json["message"].exists() {
                message = json["message"].stringValue
            }
            return APIResponse(code: code, message: message, result: result)
        }
    }
}


extension DataRequest{
    func rx_JSON(options: JSONSerialization.ReadingOptions = .allowFragments) -> Observable<JSON> {
        let observable = Observable<JSON>.create { observer in
//            if let method = self.request?.httpMethod, let urlString = self.request?.url {
////                print("[\(method)] \(urlString)")
//                if let body = self.request?.httpBody {
////                    print(NSString(data: body, encoding: String.Encoding.utf8.rawValue))
//                }
//            }
            
            self.responseJSON(options: options) { response in
                if let error = response.result.error {
                    _ = String(data: response.data!, encoding: String.Encoding.utf8)
//                    print(string)
                    observer.onError(error)
                } else if let value = response.result.value {
                    let json = JSON(value)
                    if let error = json.error {
                        observer.onError(error)
                    } else {
                        observer.onNext(json)
                        observer.onCompleted()
                    }
                    
                } else {
                    observer.onError(NSError(domain: "APIErrorDomain", code: -1, userInfo: [NSLocalizedDescriptionKey: "Unknown Error"]))
                }
            }
            return Disposables.create(with: self.cancel)
        }
        return Observable.deferred { return observable }
    }
}
