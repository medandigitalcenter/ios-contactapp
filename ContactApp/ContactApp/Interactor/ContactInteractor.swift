//
//  ContactInteractor.swift
//  ContactApp
//
//  Created by Adi Wibowo P on 6/26/19.
//  Copyright © 2019 Adi Wibowo P. All rights reserved.
//

import Foundation
import RxSwift
import SwiftyJSON

class ContactInteractor: NSObject {
    let api: APIConnector
    let disposeBag = DisposeBag()
    
    convenience override init() {
        self.init(apiConnector: APIConnector.instance)
    }
    
    init(apiConnector: APIConnector) {
        self.api = apiConnector
    }
    
    var contacts = Variable([ContactItem]())
    
    func getListContacts() -> Observable<Void> {
        return api.getListContact()
            .do ( onNext :  {
                _contacts in
                print("Nge Load Data")
                self.contacts.value = _contacts.sorted {
                    (id1, id2) -> Bool in
                    if let n1 = id1.firstName?.lowercased(), let n2 = id2.firstName?.lowercased() {
                        return n1 < n2 // Use > for Descending order
                    }
                    return true
                }
            }).map { _ in return Void() }
    }
    
    func saveContact(contact : ContactItem) -> Observable<Void> {
        return api.saveContact(contact: contact)
            .do ( onNext :  {
                print("How it is")
            }).map { _ in return Void() }
    }
    
    func updateContact(contact : ContactItem) -> Observable<ContactItem> {
        return api.updateContact(contact: contact)
            .do ( onNext :  { contact in
                print("How it is")
            }).map { contact in return contact }
    }
    
    func detailContact(contact : ContactItem) -> Observable<ContactItem> {
        return api.detailContact(contact: contact)
            .do ( onNext :  { contact in
                print("How it is")
            }).map { contact in return contact }
    }
    
    func deleteContact(contact : ContactItem) -> Observable<Void> {
        return api.deleteContact(contact: contact)
            .do ( onNext :  {
                print("How it is")
            }).map { _ in return Void() }
    }
    
}
