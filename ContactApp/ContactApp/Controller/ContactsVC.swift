//
//  ContactsVC.swift
//  ContactApp
//
//  Created by Adi Wibowo P on 6/26/19.
//  Copyright © 2019 Adi Wibowo P. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift
import AlamofireImage

class ContactsVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak private var animationViewLbl: UILabel!
    @IBOutlet weak var loadingVIew: UIActivityIndicatorView!
    
    private var navBarTitle: String = "All Contacts"
    private let refreshStream = PublishSubject<Void>()
    private let disposeBag = DisposeBag()
    let interactor = ContactInteractor()
    var tempContacs = [ContactItem]()
    
    private let searchController = UISearchController(searchResultsController: nil)
    var isSearch = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupStream()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        refreshStream.onNext(())
    }
    
    func setupView() {
        //Nav Bar
        self.navigationItem.searchController = searchController
        self.navigationItem.hidesSearchBarWhenScrolling = false
        
        
        searchController.searchResultsUpdater = self as UISearchResultsUpdating
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.title = navBarTitle
        
    }
    
    func setupStream() {
        refreshStream
            .do(onNext : { _ in
                //Loading
                self.loadingVIew.startAnimating()
                self.loadingVIew.isHidden = false
                self.animationViewLbl.text = ""
            })
            .filter{
                return true
            }
            .flatMap { url in
                self.interactor.getListContacts()
            }
            .do( onNext : { _ in
                self.loadingVIew.stopAnimating()
                self.loadingVIew.isHidden = true
                if self.interactor.contacts.value.count == 0 {
                    self.animationViewLbl.text = "No Contact"
                }else {
                    self.animationViewLbl.isHidden = true
                }
                self.tableView.reloadData()
            }, onError : {
                errorType in
                let error = errorType as NSError
                print(error)
                self.loadingVIew.stopAnimating()
                self.loadingVIew.isHidden = true
                self.animationViewLbl.text = error.description
                self.tableView.reloadData()
            })
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    @IBAction private func createContactBtnPressed(_ sender: Any) {
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        profileVC.state = .createNew
        profileVC.contact = ContactItem()
        profileVC.delegate = self
        present(profileVC, animated: true, completion: nil)
    }
    
}

extension ContactsVC : DetailPageDelegate {
    func doneFromDetail() {
        refreshStream.onNext(())
    }
}

extension ContactsVC : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        if isSearch {
            return tempContacs.count
        }else {
            return interactor.contacts.value.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var attributedText = NSAttributedString()
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as? ContactsCell else { return UITableViewCell() }
        
        let range = ("".lowercased() as NSString).range(of: ("".lowercased()))
        var fullname = ""
        if !isSearch {
                if let fn = interactor.contacts.value[indexPath.section].firstName {
                        fullname = fn + " "
                }
                if let ln = interactor.contacts.value[indexPath.section].lastName {
                        fullname += ln
                }
                
                let attributedString = NSMutableAttributedString(string: fullname)
                attributedString.addAttribute(NSAttributedString.Key.backgroundColor, value: UIColor.yellow, range: range)
                attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "AvenirNext-DemiBold", size: 20)!, range: range)
                
                attributedText = attributedString
                
                cell.contactName.attributedText = attributedText
                if let urlString = interactor.contacts.value[indexPath.section].photo, let url = NSURL(string: urlString) {
                    cell.contactImage.af_setImage(withURL: url as URL, placeholderImage: UIImage(named: "personPlaceholder"), filter: nil, progress: nil, imageTransition: .crossDissolve(0.3), runImageTransitionIfCached: true, completion: nil)
                }
        }else {
            if let fn = tempContacs[indexPath.section].firstName {
                fullname = fn + " "
            }
            if let ln = tempContacs[indexPath.section].lastName {
                fullname += ln
            }
            
            let attributedString = NSMutableAttributedString(string: fullname)
            attributedString.addAttribute(NSAttributedString.Key.backgroundColor, value: UIColor.yellow, range: range)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "AvenirNext-DemiBold", size: 20)!, range: range)
            
            attributedText = attributedString
            
            cell.contactName.attributedText = attributedText
            if let urlString = tempContacs[indexPath.section].photo, let url = NSURL(string: urlString) {
                cell.contactImage.af_setImage(withURL: url as URL, placeholderImage: UIImage(named: "personPlaceholder"), filter: nil, progress: nil, imageTransition: .crossDissolve(0.3), runImageTransitionIfCached: true, completion: nil)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        if !isSearch {
            profileVC.contact = interactor.contacts.value[indexPath.section]
        }else {
            profileVC.contact = tempContacs[indexPath.section]
        }
        profileVC.state = .view
        profileVC.delegate = self
        present(profileVC, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}


extension UIViewController {
    func showToast(message : String, font: UIFont) {
        
        let toastLabel = UILabel(frame: CGRect(x: 0, y: self.view.frame.size.height-100, width: self.view.frame.size.width, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    } }


extension ContactsVC: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContacts(text: searchController.searchBar.text!)
        
        if searchController.isActive && searchController.searchBar.text != "" {
            
        } else {
            
        }
    }
    
    private func filterContacts(text: String, scope: String = "All") {
        print("Search \(text)")
        if text.count == 0 {
            isSearch = false
            self.tableView.reloadData()
        }else {
            self.tempContacs.removeAll()
            isSearch = true
            for c in self.interactor.contacts.value {
                if let f = c.firstName, let l = c.lastName, ("\(f) \(l)").lowercased().contains(text.lowercased()) {
                    self.tempContacs.append(c)
                }
            }
            self.tableView.reloadData()
        }
    }
    
}
