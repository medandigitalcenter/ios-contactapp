//
//  ProfileVC.swift
//  ContactApp
//
//  Created by Adi Wibowo P on 6/26/19.
//  Copyright © 2019 Adi Wibowo P. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

enum ProfileTypeEnum {
    case createNew
    case view
}

protocol DetailPageDelegate {
    func doneFromDetail()
}

class ProfileVC: UIViewController {

    @IBOutlet weak private var firstNameTxtField: UITextField!
    @IBOutlet weak private var lastNameTxtField: UITextField!
    @IBOutlet weak private var dateOfBirthTextField: UITextField!
    @IBOutlet weak private var modifyBtn: UIButton!
    @IBOutlet weak private var nameLbl: UILabel!
    
    @IBOutlet weak private var saveBtn: UIButton!
    @IBOutlet weak private var cancelBtn: UIButton!
    @IBOutlet weak private var backBtn: UIButton!
    @IBOutlet weak private var profileImg: UIImageView!
    @IBOutlet weak private var changeImageBtn: UIButton!
    @IBOutlet weak var btnDelete: CustomButton!
    
    var contact = ContactItem()
    var state : ProfileTypeEnum = .view
    var delegate : DetailPageDelegate! 
    
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    let interactor = ContactInteractor()
    
    private let createStream = PublishSubject<Void>()
    private let updateStream = PublishSubject<Void>()
    private let deleteStream = PublishSubject<Void>()
    private let detailStream = PublishSubject<Void>()
    
    
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initDetailProfile()
        setupStream()
        self.loadingView.isHidden = true
        
        if self.state == .view {
            detailStream.onNext(())
        }
    }
    
    @IBAction private func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.delegate.doneFromDetail()
        })
    }
    
    @IBAction private func modifyProfileBtnPressed(_ sender: Any) {
            self.modifyBtn.isHidden = true
            self.saveBtn.isHidden = false
            self.backBtn.isHidden = true
            self.cancelBtn.isHidden = false
            self.nameLbl.isHidden = true
            disEnd(flag: false)
    }
    
    @IBAction func deleteButtonPressed(_ sender: Any) {
        deleteStream.onNext(())
    }
    
    func disEnd(flag : Bool){
        self.lastNameTxtField.isHidden = flag
        self.dateOfBirthTextField.isHidden = flag
        self.firstNameTxtField.isHidden = flag
    }
    
    @IBAction private func cancelBtnPressed(_ sender: Any) {
            initDetailProfile()
        self.cancelBtn.isHidden = true
        self.backBtn.isHidden = false
    }
    
    func setupStream(){
        self.firstNameTxtField.delegate = self
        self.lastNameTxtField.delegate = self
        self.dateOfBirthTextField.delegate = self
        
        createStream
            .do(onNext : { _ in
                //Loading
                self.loadingView.startAnimating()
                self.loadingView.isHidden = false
            })
            .filter{
                self.loadingView.stopAnimating()
                self.loadingView.isHidden = true
                
                if (self.firstNameTxtField.text?.isEmpty)! {
                    return false
                }else {
                    self.contact.firstName = self.firstNameTxtField.text
                }
                
                if (self.lastNameTxtField.text?.isEmpty)! {
                    return false
                }else {
                    self.contact.lastName = self.lastNameTxtField.text
                }
                
                if (self.dateOfBirthTextField.text?.isEmpty)! {
                    return false
                }else {
                    if let a = Int(self.dateOfBirthTextField.text!){
                        self.contact.age = a
                    }
                }
                
                self.loadingView.startAnimating()
                self.loadingView.isHidden = false
                
                return true
            }
            .flatMap { url in
                self.interactor.saveContact(contact: self.contact)
            }
            .do( onNext : { _ in
                self.loadingView.stopAnimating()
                self.loadingView.isHidden = true
                self.state = .view
                self.initDetailProfile()
                self.loadingView.stopAnimating()
                self.loadingView.isHidden = true
                
                self.modifyBtn.isHidden = true
                self.saveBtn.isHidden = true
                self.backBtn.isHidden = false
                self.cancelBtn.isHidden = true
                self.nameLbl.isHidden = false
                self.disEnd(flag: true)
                
                self.dismiss(animated: true, completion: {
                    self.delegate.doneFromDetail()
                })
                
            }, onError : {
                errorType in
                let error = errorType as NSError
                print(error)
                self.showToast(message: error.localizedDescription, font: UIFont(name: "AvenirNext-DemiBold", size: 10)!)
                self.loadingView.stopAnimating()
                self.loadingView.isHidden = true
            })
            .subscribe()
            .disposed(by: disposeBag)
        
        updateStream
            .do(onNext : { _ in
                //Loading
                self.view.endEditing(true)
                self.loadingView.startAnimating()
                self.loadingView.isHidden = false
            })
            .filter{
                self.loadingView.stopAnimating()
                self.loadingView.isHidden = true
                
                if (self.firstNameTxtField.text?.isEmpty)! {
                    return false
                }else {
                    self.contact.firstName = self.firstNameTxtField.text
                }
                
                if (self.lastNameTxtField.text?.isEmpty)! {
                    return false
                }else {
                    self.contact.lastName = self.lastNameTxtField.text
                }
                
                if (self.dateOfBirthTextField.text?.isEmpty)! {
                    return false
                }else {
                    if let a = Int(self.dateOfBirthTextField.text!){
//                        if a > 100 {
//                            self.showToast(message: "Age can't be older than 100", font: UIFont(name: "AvenirNext-DemiBold", size: 14)!)
//                            return false
//                        }
                        self.contact.age = a
                    }
                }
                
                self.loadingView.startAnimating()
                self.loadingView.isHidden = false
                
                return true
            }
            .flatMap { url in
                self.interactor.updateContact(contact: self.contact)
            }
            .do( onNext : { contact in
                self.view.endEditing(true)
                self.contact = contact
                self.initDetailProfile()
                self.loadingView.stopAnimating()
                self.loadingView.isHidden = true
                
                self.modifyBtn.isHidden = false
                self.saveBtn.isHidden = true
                self.backBtn.isHidden = false
                self.cancelBtn.isHidden = true
                self.nameLbl.isHidden = false
                self.disEnd(flag: true)
                
            }, onError : {
                errorType in
                let error = errorType as NSError
                print(error)
                self.showToast(message: error.localizedDescription, font: UIFont(name: "AvenirNext-DemiBold", size: 10)!)
                self.loadingView.stopAnimating()
                self.loadingView.isHidden = true
            })
            .retry()
            .subscribe()
            .disposed(by: disposeBag)
        
        
        
        deleteStream
            .do(onNext : { _ in
                //Loading
                self.loadingView.startAnimating()
                self.loadingView.isHidden = false
                self.btnDelete.setTitle("   Deleting   ", for: UIControlState.normal)
            })
            .filter{
                return true
            }
            .flatMap { url in
                self.interactor.deleteContact(contact: self.contact)
            }
            .do( onNext : { contact in
                self.contact = ContactItem()
                self.initDetailProfile()
                self.loadingView.stopAnimating()
                self.loadingView.isHidden = true
                
                self.modifyBtn.isHidden = false
                self.saveBtn.isHidden = true
                self.backBtn.isHidden = false
                self.cancelBtn.isHidden = true
                self.nameLbl.isHidden = false
                self.disEnd(flag: true)
                self.btnDelete.setTitle("   Delete Contact   ", for: UIControlState.normal)
                self.dismiss(animated: true, completion: {
                    self.delegate.doneFromDetail()
                })
                
            }, onError : {
                errorType in
                let error = errorType as NSError
                print(error)
                self.btnDelete.setTitle("   Delete Contact   ", for: UIControlState.normal)
                self.showToast(message: error.localizedDescription, font: UIFont(name: "AvenirNext-DemiBold", size: 14)!)
                self.loadingView.stopAnimating()
                self.loadingView.isHidden = true
            })
            .retry()
            .subscribe()
            .disposed(by: disposeBag)
        
        detailStream
            .do(onNext : { _ in
                //Loading
                print("Load Detail ")
                self.view.endEditing(true)
                self.loadingView.startAnimating()
                self.loadingView.isHidden = false
            })
            .filter{
                return true
            }
            .flatMap { url in
                self.interactor.detailContact(contact: self.contact)
            }
            .do( onNext : { contact in
                self.showToast(message: "Detail Loaded", font: UIFont(name: "AvenirNext-DemiBold", size: 10)!)
                self.view.endEditing(true)
                self.contact = contact
                self.initDetailProfile()
                self.loadingView.stopAnimating()
                self.loadingView.isHidden = true
                
                self.modifyBtn.isHidden = false
                self.saveBtn.isHidden = true
                self.backBtn.isHidden = false
                self.cancelBtn.isHidden = true
                self.nameLbl.isHidden = false
                self.disEnd(flag: true)
                
            }, onError : {
                errorType in
                let error = errorType as NSError
                print(error)
                self.showToast(message: error.localizedDescription, font: UIFont(name: "AvenirNext-DemiBold", size: 10)!)
                self.loadingView.stopAnimating()
                self.loadingView.isHidden = true
            })
            .retry()
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    @IBAction private func saveBtnPressed(_ sender: Any) {
        if self.state == .createNew {
            createStream.onNext(())
        }else {
            updateStream.onNext(())
        }
        
    }
    
    func initDetailProfile(){
        var fullIdent = ""
        self.changeImageBtn.isHidden = true
        
        if let fN = contact.firstName {
            self.firstNameTxtField.text = fN
            fullIdent += fN
        }
        if let lN = contact.lastName {
            self.lastNameTxtField.text = lN
            fullIdent += " " + lN
        }
        if contact.age > 0 {
            let temp = contact.age > 1 ? "\(contact.age) years old" : "\(contact.age) year old"
            fullIdent += "\n" + temp
            self.dateOfBirthTextField.text = "\(contact.age)"
        }
        
        switch state {
            case .createNew:
                disEnd(flag: false)
                self.modifyBtn.isHidden = true
                self.saveBtn.isHidden = false
                self.btnDelete.isHidden = true
            
            case .view :
                disEnd(flag: true)
                self.modifyBtn.isHidden = false
                self.saveBtn.isHidden = true
                self.nameLbl.isHidden = false
                self.btnDelete.isHidden = false
                
                self.nameLbl.text = fullIdent
                if let urlString = contact.photo, let url = NSURL(string: urlString) {
                    self.profileImg.af_setImage(withURL: url as URL, placeholderImage: UIImage(named: "personPlaceholder"), filter: nil, progress: nil, imageTransition: .crossDissolve(0.3), runImageTransitionIfCached: true, completion: nil)
            }
        }
    }
    
}

extension ProfileVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.dateOfBirthTextField {
            guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
                return false
            }
            return true
        }
        return true
    }
}


