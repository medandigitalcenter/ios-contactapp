# README #


This Contact App runs for iOS Mobile as Native application, completed with CRUD & Search Feature. 


### What is this repository for? ###

* Jenius Frontend Test 

### How do I get set up? ###

* Clone this repository
* Use XCode 10.1 & Swift 4
* Rest API by Alamofire
* Optimized by RxSwift
* VIPER Pattern with clean code & no warning

### .app Installation ###
* Download .app file from https://drive.google.com/open?id=1gFhCKHMkXwidQ7LFG8aosHuRgpMm1N9-
* Unarchive that package
* Connect your device to your PC.
* Open Xcode, go to Window → Devices .
* Then, the Devices screen will appear. Choose the device you want to install the app on.
* Drag and drop your .app file into the Installed Apps:
* Setting trust developer from device management on iPhone Settings. 

### Who do I talk to? ###

* Any question can be asked to me : adiwibowoplus@gmail.com